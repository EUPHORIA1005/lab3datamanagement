import psycopg2


def connect(userName=None, password=None):
    conn = None
    try:
        conn = psycopg2.connect(
            host="localhost",
            database="postgres",
            user=userName,
            password=password
        )
    except psycopg2.Error:
        print("Could not connect")
        return None
    return conn


def check_role():
    role = input("Введите роль (Administrator/Guest): ")
    if role.lower() not in ["administrator", "guest"]:
        print("Неверная роль. Попробуйте снова.")
        return check_role()
    return role.lower()


def create_table(role, userName, password):
    if role != "administrator":
        print("Недостаточно прав для выполнения данной операции.")
        

    tablename = input("Введите название таблицы для создания: ") 
    conn = connect(userName=userName, password=password)
    if conn is None:
        return

    cursor = conn.cursor()
    cursor.execute(f'''
        CREATE OR REPLACE FUNCTION create_table()
        RETURNS VOID AS $$
        BEGIN
            CREATE TABLE IF NOT EXISTS {tablename}  
            (
                id SERIAL PRIMARY KEY,
                name VARCHAR(30) NOT NULL,
                age int NOT NULL
            );
        END;
        $$ LANGUAGE plpgsql;

        SELECT create_table();
    ''')
    conn.commit()
    cursor.close()
    conn.close()
    print("Таблица создана")


def delete_table(role, userName, password):
    if role != "administrator":
        print("Недостаточно прав для выполнения данной операции.")

    tablename = input("Введите название таблицы для удаления: ")  
    conn = connect(userName=userName, password=password)
    if conn is None:
        return

    cursor = conn.cursor()
    cursor.execute(f'''
        CREATE OR REPLACE FUNCTION delete_table()
        RETURNS VOID AS $$
        BEGIN
            DROP TABLE IF EXISTS {tablename};  
        END;
        $$ LANGUAGE plpgsql;

        SELECT delete_table();
    ''')
    conn.commit()
    cursor.close()
    conn.close()
    print("Таблица удалена")


def clear_table(role, userName, password):
    if role != "administrator":
        print("Недостаточно прав для выполнения данной операции.")

    tablename = input("Введите название таблицы для очистки: ")  
    conn = connect(userName=userName, password=password)
    if conn is None:
        return

    cursor = conn.cursor()
    cursor.execute(f'''
            CREATE OR REPLACE FUNCTION clear_table()
            RETURNS VOID AS $$
            BEGIN
                DELETE FROM {tablename};  -- Используем введенное название таблицы
            END;
            $$ LANGUAGE plpgsql;

            SELECT clear_table();
        ''')
    conn.commit()
    cursor.close()
    conn.close()
    print("Таблица очищена")


def search_data(role, userName, password):
    tablename = input("Введите имя таблицы: ")
    search_val = input("Введите имя для поиска строки с таким именем: ")

    conn = connect(userName=userName, password=password)
    cursor = conn.cursor()
    cursor.execute("""
        CREATE OR REPLACE FUNCTION searchByName(table_name varchar, search_val varchar)
        RETURNS TABLE (
            id int,
            name varchar(30),
            age int
        )
        AS $$
        BEGIN
            RETURN QUERY EXECUTE FORMAT('SELECT * FROM %I WHERE name ILIKE %L', table_name, search_val);
        END;
        $$ LANGUAGE plpgsql;
    """)

    cursor.callproc('searchByName', (tablename, search_val))
    results = cursor.fetchall()
    for row in results:
        print(row)

    cursor.close()
    conn.close()


def login():
    username = input("Введите имя пользователя: ")
    password = input("Введите пароль: ")

    if username == "postgres" and password == "root":
        return "administrator", username, password
    elif username == "user" and password == "user":
        return "guest", username, password
    else:
        print("Неверное имя пользователя или пароль. Попробуйте снова.")
        return login()


def viewtable(role, userName, password):
    tablename = input("Введите имя таблицы: ")

    conn = connect(userName=userName, password=password)
    cursor = conn.cursor()
    cursor.execute('''
        CREATE OR REPLACE FUNCTION view_table(tablename VARCHAR)
        RETURNS TABLE (
            id INT,
            name VARCHAR(30),
            age INT
        )
        AS $$
        BEGIN
            RETURN QUERY EXECUTE FORMAT('SELECT * FROM %I', tablename);
        END;
        $$ LANGUAGE plpgsql;
    ''')

    cursor.callproc('view_table', [tablename])
    results = cursor.fetchall()
    for row in results:
        print(row)

    cursor.close()
    conn.close()


def insert_data(role, userName, password):
    if role != "administrator":
        print("Недостаточно прав для выполнения данной операции.")

    tab_name = input("Введите имя таблицы: ")
    id_val = int(input("Введите id: "))
    name_val = input("Введите name: ")
    age_val = int(input("Введите age: "))

    conn = connect(userName=userName, password=password)
    if conn is None:
        return

    cursor = conn.cursor()
    cursor.execute('''
        CREATE OR REPLACE FUNCTION insert_table(tab_name varchar(30), id_val int, name_val varchar(30),
           age_val int)
        RETURNS void
        AS
        $$
        BEGIN
            EXECUTE FORMAT('INSERT INTO %I VALUES (%s, %L,  %s)', tab_name, id_val, name_val, age_val);
        END;
        $$
        LANGUAGE plpgsql;
    ''')

    cursor.callproc("insert_table", [tab_name, id_val, name_val, age_val])
    conn.commit()
    cursor.close()
    conn.close()

    print("Данные успешно добавлены")


def update_data(role, userName, password):
    if role != "administrator":
        print("Недостаточно прав для выполнения данной операции.")

    tab_name = input("Введите имя таблицы: ")
    id_val = int(input("Введите id: "))
    name_val = input("Введите name: ")
    age_val = int(input("Введите age: "))

    conn = connect(userName=userName, password=password)
    if conn is None:
        return

    cursor = conn.cursor()
    cursor.execute('''
        CREATE OR REPLACE FUNCTION update_table(tab_name varchar(30), id_val int, name_val varchar(30),
           age_val int)
        RETURNS void AS
        $$
        BEGIN
            EXECUTE FORMAT('UPDATE %I SET name = %L, age = %s WHERE id = %s',
                           tab_name, name_val, age_val, id_val);
        END;
        $$
        LANGUAGE plpgsql;
    ''')

    cursor.callproc("update_table", [tab_name, id_val, name_val, age_val])
    conn.commit()
    cursor.close()
    conn.close()

    print("Данные успешно обновлены")



def main():
    role, userName, password = login()
    print(f"Вы вошли в систему как {role.capitalize()}")
    try:
        while True:
            print("\nВыберите действие:")
            print("1. Создать таблицу")
            print("2. Удалить таблицу")
            print("3. Очистить таблицу")
            print("4. Поиск данных")
            print("5. Посмотреть содержимое таблицы")
            print("6. Обновить запись в таблице")
            print("7. Добавить запись в таблицу")
            print("0. Выйти из программы")

            choice = input("Введите номер действия: ")
            if choice == "1":
                create_table(role, userName, password)
            elif choice == "2":
                delete_table(role, userName, password)
            elif choice == "3":
                clear_table(role, userName, password)
            elif choice == "4":
                search_data(role, userName, password)
            elif choice == "5":
                viewtable(role, userName, password)
            elif choice == "6":
                update_data(role, userName, password)
            elif choice == "7":
                insert_data(role, userName, password)
            elif choice == "0":
                print("Выход из программы.")
                break
            else:
                print("Неверный выбор. Попробуйте снова.")
    except(psycopg2.errors.InsufficientPrivilege):
        print("Ошибка из бд - недостаточно прав")
if __name__ == "__main__":
    main()
